pragma solidity >=0.5.16;

contract Resolver {

	string public name;
	bool public init;

	struct location {
		string value;
		bytes head;
		bool inactive;
	}

	struct member {
		bytes32 head;
		uint256[] locations;
	}

	uint256 memberCursor;
	mapping(address => uint256) public members;
	mapping(uint256 => member) public memberIndex;

	location[] locations;

	event NewMember(address addr, uint256 idx);
	event NewLocation(address addr, uint256 idx, bytes32 sum);
	event NewUpdate(address addr, bytes32 update);
	event NewLocationUpdate(address indexed addr, uint256 idx, bytes update);

	constructor(string memory _name) public {
		name = _name;
		init = true;
		_register(msg.sender); // contract owner cannot add locations, see isRegistered
	}

	function register() public {
		_register(msg.sender);
	}
	
	function _register(address _addr) private {
		require(!isRegistered(msg.sender));

		uint256 cursor;
		member memory newMember;

		cursor = memberCursor;
		memberCursor++;
		memberIndex[cursor] = newMember;
		members[_addr] = cursor;
		emit NewMember(_addr, cursor);
	}

	function update(bytes32 _head) public {
		uint256 updateMemberIndex;

		updateMemberIndex = members[msg.sender];
		member storage updateMember = memberIndex[updateMemberIndex];
		updateMember.head = _head;

		emit NewUpdate(msg.sender, _head);
	}

	function getUpdate(address _addr) public view returns (bytes32) {
		require(isRegistered(_addr));

		uint256 updateMemberIndex;
		member memory updateMember;

		updateMemberIndex = members[_addr];
		updateMember = memberIndex[updateMemberIndex];
	       	return updateMember.head;
	}

	function updateLocation(uint256 _idx, bytes memory _head) public {
		require(isRegistered(msg.sender));

		uint256 updateMemberIndex;
		member memory updateMember;
		uint256 locIdx;

		updateMemberIndex = members[msg.sender];
		updateMember = memberIndex[updateMemberIndex];

		locIdx = updateMember.locations[_idx];
		location storage loc = locations[locIdx];
		loc.head = _head;

		emit NewLocationUpdate(msg.sender, _idx, _head);
	}
	
	function addLocation(string memory _location) public {
		require(isRegistered(msg.sender));

		bytes32 locHash;
		uint256 locIdx;
		uint256 memberIdx;

		locIdx = locations.length;	
		location memory loc = location(_location, "0x0", false);
		locations.push(loc);
		loc.value = _location;

		memberIdx = members[msg.sender];
		member storage contextMember = memberIndex[memberIdx];
		contextMember.locations.push(locIdx);
			
		locHash = sha256(bytes(_location));
		emit NewLocation(msg.sender, locIdx, locHash);
	}

	function getLocation(address _addr, uint256 _idx) public view returns(string memory, bytes memory) {
		require(isRegistered(_addr));

		uint256 locIdx;
		uint256 memberIdx;

		memberIdx = members[_addr];
		member storage contextMember = memberIndex[memberIdx];
		locIdx = contextMember.locations[_idx];

		return (locations[locIdx].value, locations[locIdx].head);
	}

	function isRegistered(address _address) public view returns (bool) {
		return members[_address] != 0;
	}
}
